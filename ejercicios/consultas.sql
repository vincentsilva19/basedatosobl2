-- EJERCICIO 1

SELECT em.tipodoc, em.nroemp
FROM empleado em
JOIN menu mn
	ON em.tipodoc = mn.tipodoc AND em.nroemp = mn.nroemp
JOIN pedido ped
	ON ped.codmenu = mn.codmenu
WHERE
	ped.tipo = 'Delivery'
MINUS
SELECT em2.tipodoc, em2.nroemp
FROM empleado em2
JOIN menu mn2
	ON em2.tipodoc = mn2.tipodoc AND em2.nroemp = mn2.nroemp
JOIN pedido ped2
	ON ped2.codmenu = mn2.codmenu
WHERE
	ped2.tipo = 'Local';

-- EJERCICIO 2

SELECT em.*
FROM pedido p1 
LEFT JOIN pedido p2 
	ON p1.montoped<p2.montoped
JOIN empleado em
	ON p1.tipodoc = em.tipodoc and p1.nroemp = em.nroemp
WHERE
	p2.montoped is null;

-- EJERCICIO 3

SELECT *
FROM empleado emp
JOIN pedido ped
    ON emp.tipodoc = ped.tipodoc AND emp.nroemp = ped.nroemp
JOIN menu mn
    ON ped.codmenu = mn.codmenu
JOIN partede prt
    ON prt.codmenu = mn.codmenu
JOIN menu mnp
    ON prt.codmenucomponente = mnp.codmenu
WHERE
    mnp.descripcion = 'salsa bechamel'
    AND ped.tipo = 'Local';

-- EJERCICIO 6
SELECT
    mn.codmenu,
    mn.nombre,
    mn.descripcion,
    CASE WHEN COUNT(pt.codmenucomponente) > 0 THEN 
        LISTAGG(pt.codmenucomponente, ', ') WITHIN GROUP (ORDER BY pt.codmenucomponente) 
    ELSE 
        'no tiene' 
    END "componentes"
FROM menu mn
LEFT OUTER JOIN partede pt
    ON mn.codmenu = pt.codmenu
GROUP BY mn.codmenu, mn.nombre, mn.descripcion;


-- EJERCICIO 9

SELECT em.*, pt.codmenu, pt.codmenucomponente, mnc.nroemp
FROM empleado em
JOIN menu mn
    ON em.tipodoc = mn.tipodoc AND em.nroemp = mn.nroemp
JOIN partede pt
    ON mn.codmenu = pt.codmenu
JOIN menu mnc
    ON pt.codmenucomponente = mnc.codmenu AND em.tipodoc = mnc.tipodoc AND em.nroemp = mnc.nroemp
WHERE
    mn.codmenu IN (
        SELECT ped.codmenu
        FROM pedido ped
        WHERE
            ped.fecha < '12/12/2019'
        GROUP BY ped.codmenu
        HAVING COUNT(ped.codmenu) = (
            SELECT MAX(COUNT(mped.codmenu)) 
            FROM pedido mped 
            WHERE mped.fecha < '12/12/2019' 
            GROUP BY mped.codmenu
        )
    )
    AND mnc.codmenu IN (
        SELECT ped.codmenu
        FROM pedido ped
        WHERE
            ped.fecha < '12/12/2019'
        GROUP BY ped.codmenu
        HAVING COUNT(ped.codmenu) = (
            SELECT MIN(COUNT(mped.codmenu)) 
            FROM pedido mped 
            WHERE mped.fecha < '12/12/2019' 
            GROUP BY mped.codmenu
        )
    )
;


SELECT em.nroemp, pt.codmenu, pt.codmenucomponente, COUNT(ped.codmenu)
FROM empleado em
JOIN menu mn
    ON em.tipodoc = mn.tipodoc AND em.nroemp = mn.nroemp
JOIN partede pt
    ON mn.codmenu = pt.codmenu
JOIN menu mnc
    ON pt.codmenucomponente = mnc.codmenu AND em.tipodoc = mnc.tipodoc AND em.nroemp = mnc.nroemp
JOIN pedido ped
    ON mn.codmenu = ped.codmenu
JOIN pedido pedc
    ON mnc.codmenu = pedc.codmenu
GROUP BY em.nroemp, pt.codmenu, pt.codmenucomponente
;


-- EJERCICIO 10
SELECT
    pedpmen
FROM (
    SELECT
        estacion,
        codmenu,
        count(codmenu) AS CantPedMenu
    FROM (
        SELECT
            codmenu,
            CASE
                WHEN fecha BETWEEN TO_DATE('20/03/'||EXTRACT(YEAR FROM fecha)) AND TO_DATE('21/06/'||EXTRACT(YEAR FROM fecha)) 
                    THEN 'Otoño'
                WHEN fecha BETWEEN TO_DATE('21/6/'||EXTRACT(YEAR FROM fecha)) AND TO_DATE('23/09/'||EXTRACT(YEAR FROM fecha)) 
                    THEN 'Invierno'
            END AS estacion
        FROM pedido
        WHERE
            fecha BETWEEN TO_DATE('20/03/'||EXTRACT(YEAR FROM fecha)) AND TO_DATE('23/09/'||EXTRACT(YEAR FROM fecha)) 
        )
    GROUP BY estacion, codmenu
) pedpmen
;

/*
SELECT
    ped.estacion,
    COUNT(*) AS CantTotPed
FROM (
    SELECT
        codmenu,
        CASE
            WHEN fecha BETWEEN TO_DATE('21/03/2019') AND TO_DATE('21/06/2019') THEN 'Otoño'
            WHEN fecha BETWEEN TO_DATE('21/06/2019') AND TO_DATE('21/09/2019') THEN 'Invierno'
        END AS estacion
    FROM pedido
    WHERE
        fecha BETWEEN TO_DATE('21/03/2019') AND TO_DATE('21/09/2019')
) ped
GROUP BY ped.estacion;

SELECT
    *
FROM
(
    SELECT
        ped.estacion,
        COUNT(*) AS CantTotPed
    FROM ( 
        SELECT
            CASE
                WHEN ped.fecha BETWEEN TO_DATE('21/03/2019') AND TO_DATE('21/06/2019') THEN 'Otoño'
                WHEN ped.fecha BETWEEN TO_DATE('21/06/2019') AND TO_DATE('21/09/2019') THEN 'Invierno'
            END AS estacion
        FROM pedido ped
        WHERE
            ped.fecha BETWEEN TO_DATE('21/03/2019') AND TO_DATE('21/09/2019')
    ) ped
    GROUP BY ped.estacion
) pedpest
JOIN
(
    SELECT
        pedmn.estacion,
        pedmn.codmenu,
        COUNT(*) AS CantPedMenu
    FROM ( 
        SELECT
            ped.fecha, 
            ped.codmenu,
            CASE
                WHEN ped.fecha BETWEEN TO_DATE('21/03/2019') AND TO_DATE('21/06/2019') THEN 'Otoño'
                WHEN ped.fecha BETWEEN TO_DATE('21/06/2019') AND TO_DATE('21/09/2019') THEN 'Invierno'
            END AS estacion
        FROM pedido ped
        WHERE
            ped.fecha BETWEEN TO_DATE('21/03/2019') AND TO_DATE('21/09/2019')
    ) pedmn
    GROUP BY pedmn.estacion, pedmn.codmenu
) pedpmenu
    ON pedpest.estacion = pedpmenu.estacion
;

SELECT
        pedmn.estacion,
        pedmn.codmenu,
        COUNT(*) AS CantPedMenu
    FROM ( 
        SELECT
            ped.fecha, 
            ped.codmenu,
            CASE
                WHEN ped.fecha BETWEEN TO_DATE('21/03/2019') AND TO_DATE('21/06/2019') THEN 'Otoño'
                WHEN ped.fecha BETWEEN TO_DATE('21/06/2019') AND TO_DATE('21/09/2019') THEN 'Invierno'
            END AS estacion
        FROM pedido ped
        WHERE
            ped.fecha BETWEEN TO_DATE('21/03/2019') AND TO_DATE('21/09/2019')
    ) pedmn
    GROUP BY pedmn.estacion, pedmn.codmenu;
    */