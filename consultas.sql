-- EMPLEADO(TIPODOC, NROEMP, NOMBRE, FINGRESO);
-- MESA(NROMESA, SECTOR);
-- MENU(CODMENU, NOMBRE, DESCRIPCION,TIPODOC, NROEMP);
-- TARJETA(NROTARJ, NOMTARJ, TIPO);
-- EMPLEADOMESA(TIPODOC, NROEMP, NROMESA);
-- PEDIDO(NROTARJ, NOMTARJ, FECHA, CODMENU , MONTOPED , TIPO ('Local', 'Delivery', 'TakeAway')), TIPODOC, NROEMP);
-- PARTEDE(CODMENU, CODMENUCOMPONENTE)

-- select * from empleado em join menu mn on em.tipodoc = mn.tipodoc and em.nroemp = mn.nroemp join pedido ped on ped.codmenu = mn.codmenu order by em.nroemp;
/*
SELECT em.tipodoc, em.nroemp FROM empleado em
JOIN menu mn
    ON em.tipodoc = mn.tipodoc AND em.nroemp = mn.nroemp
JOIN pedido ped
    ON ped.codmenu = mn.codmenu
WHERE
    ped.tipo = 'Delivery'
MINUS
SELECT em2.tipodoc, em2.nroemp FROM empleado em2
JOIN menu mn2
    ON em2.tipodoc = mn2.tipodoc AND em2.nroemp = mn2.nroemp
JOIN pedido ped2
    ON ped2.codmenu = mn2.codmenu
WHERE
    ped2.tipo = 'Local'
;

SELECT * FROM empleado em
JOIN menu mn
    ON em.tipodoc = mn.tipodoc AND em.nroemp = mn.nroemp
JOIN pedido ped
    ON ped.codmenu = mn.codmenu
WHERE
    ped.tipo = 'Delivery'
    AND NOT EXISTS
    (
        SELECT * FROM empleado em2
        JOIN menu mn2
            ON em2.tipodoc = mn2.tipodoc AND em2.nroemp = mn2.nroemp
        JOIN pedido ped2
            ON ped2.codmenu = mn2.codmenu
        WHERE
            em.tipodoc = em2.tipodoc
            AND em.nroemp = em2.nroemp
            AND ped2.tipo = 'Local'
    );*/

SELECT * FROM empleado em
WHERE 
    EXISTS (
        SELECT * FROM menu mn
        JOIN pedido ped
            ON ped.codmenu = mn.codmenu
        WHERE
            mn.tipodoc = em.tipodoc
            AND mn.nroemp = em.nroemp
            AND ped.tipo = 'Delivery'
    );

SELECT * FROM empleado em
WHERE 
    EXISTS (
        SELECT * FROM menu mn
        JOIN pedido ped
            ON ped.codmenu = mn.codmenu
        WHERE
            mn.tipodoc = em.tipodoc
            AND mn.nroemp = em.nroemp
            AND ped.tipo = 'Local'
    );

SELECT * FROM empleado em
WHERE 
    EXISTS (
        SELECT * FROM menu mn
        JOIN pedido ped
            ON ped.codmenu = mn.codmenu
        WHERE
            mn.tipodoc = em.tipodoc
            AND mn.nroemp = em.nroemp
            AND ped.tipo = 'Delivery'
    )
    
    AND NOT EXISTS (
        SELECT * FROM menu mn
        JOIN pedido ped
            ON ped.codmenu = mn.codmenu
        WHERE
            mn.tipodoc = em.tipodoc
            AND mn.nroemp = em.nroemp
            AND ped.tipo = 'Local'
    );

SELECT em.tipodoc, em.nroemp FROM empleado em
JOIN menu mn
    ON em.tipodoc = mn.tipodoc AND em.nroemp = mn.nroemp
JOIN pedido ped
    ON ped.codmenu = mn.codmenu
WHERE
    ped.tipo = 'Delivery'
MINUS
SELECT em2.tipodoc, em2.nroemp FROM empleado em2
JOIN menu mn2
    ON em2.tipodoc = mn2.tipodoc AND em2.nroemp = mn2.nroemp
JOIN pedido ped2
    ON ped2.codmenu = mn2.codmenu
WHERE
    ped2.tipo = 'Local'
;

select * from pedido;