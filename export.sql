ALTER SESSION SET NLS_DATE_FORMAT='DD/MM/YYYY';
DECLARE
    rowType BOOLEAN := TRUE;
BEGIN
    FOR i IN 1..2 LOOP
        IF NOT rowType THEN
            DBMS_OUTPUT.PUT_LINE('-------------------------------------------------------------------------------');
            DBMS_OUTPUT.PUT_LINE('group: db2');
            DBMS_OUTPUT.PUT_LINE('empleado = {tipodoc:string, nroemp:number, nombre:string, fingreso:string');
        ELSE
            DBMS_OUTPUT.PUT_LINE('DELETE FROM EMPLEADO;');
            DBMS_OUTPUT.PUT_LINE('DELETE FROM PEDIDO;');
            DBMS_OUTPUT.PUT_LINE('DELETE FROM EMPLEADOMESA;');
            DBMS_OUTPUT.PUT_LINE('DELETE FROM TARJETA;');
            DBMS_OUTPUT.PUT_LINE('DELETE FROM MENU;');
            DBMS_OUTPUT.PUT_LINE('DELETE FROM MESA;');
            DBMS_OUTPUT.PUT_LINE('DELETE FROM EMPLEADO;');
            DBMS_OUTPUT.PUT_LINE()
        END IF;
        FOR campo IN (select * from empleado) LOOP
            IF rowType THEN
                DBMS_OUTPUT.PUT_LINE('  INTO EMPLEADO (TIPODOC, NROEMP, NOMBRE, FINGRESO) VALUES ('''||campo.tipodoc||''','||campo.nroemp||', '''||campo.nombre||''', '''||campo.fingreso||''')');
            ELSE
                DBMS_OUTPUT.PUT_LINE('"' || campo.tipodoc || '", ' || campo.nroemp || ', "' || campo.nombre || '", "' || campo.fingreso || '"');
            END IF;
        END LOOP;
        IF NOT rowType THEN
            DBMS_OUTPUT.PUT_LINE('}');
            DBMS_OUTPUT.PUT_LINE('mesa = {nromesa:number, sector:string');
        END IF;
        FOR fila IN (select * from mesa) LOOP
            IF rowType THEN
                DBMS_OUTPUT.PUT_LINE('INSERT INTO MESA (NROMESA, SECTOR) VALUES ('||fila.nromesa||', '''||fila.sector||''');');
            ELSE
                DBMS_OUTPUT.PUT_LINE(fila.nromesa || ', "' || fila.sector || '"');
            END IF;
        END LOOP;
        IF NOT rowType THEN
            DBMS_OUTPUT.PUT_LINE('}');
            DBMS_OUTPUT.PUT_LINE('menu = {codmenu:number, nombre:string, descripcion:string, tipodoc:string, nroemp:number');
        END IF;
        FOR fila IN (select * from menu) LOOP
            IF rowType THEN
                DBMS_OUTPUT.PUT_LINE('INSERT INTO MENU (CODMENU, NOMBRE, DESCRIPCION, TIPODOC, NROEMP) VALUES ('||fila.codmenu||', '''||fila.nombre||''', '''||fila.descripcion||''', '''||fila.tipodoc||''', '||fila.nroemp||');');
            ELSE
                DBMS_OUTPUT.PUT_LINE(fila.codmenu||', '''||fila.nombre||''', '''||fila.descripcion||''', '''||fila.tipodoc||''', '||fila.nroemp);
            END IF;
        END LOOP;
        IF NOT rowType THEN
            DBMS_OUTPUT.PUT_LINE('}');
            DBMS_OUTPUT.PUT_LINE('tarjeta = {nrotarj:number, nomtarj:string, tipo:string');
        END IF;
        FOR fila IN (SELECT * FROM TARJETA) LOOP
            IF rowType THEN
                DBMS_OUTPUT.PUT_LINE('INSERT INTO TARJETA (NROTARJ, NOMTARJ, TIPO) VALUES ('||fila.nrotarj||', '''||fila.nomtarj||''', '''||fila.tipo||''');');
            ELSE
                DBMS_OUTPUT.PUT_LINE(fila.nrotarj||', '''||fila.nomtarj||''', '''||fila.tipo||'''');
            END IF;
        END LOOP;
        IF NOT rowType THEN
            DBMS_OUTPUT.PUT_LINE('}');
            DBMS_OUTPUT.PUT_LINE('empleadomesa = {tipodoc:string, nroemp:number, nromesa:number');
        END IF;
        FOR fila IN (SELECT * FROM EMPLEADOMESA) LOOP
            IF rowType THEN
                DBMS_OUTPUT.PUT_LINE('INSERT INTO EMPLEADOMESA (TIPODOC, NROEMP, NROMESA) VALUES ('''||fila.tipodoc||''', '||fila.nroemp||', '||fila.nromesa||');');
            ELSE
                DBMS_OUTPUT.PUT_LINE(''''||fila.tipodoc||''', '||fila.nroemp||', '||fila.nromesa);
            END IF;
        END LOOP;
        IF NOT rowType THEN
            DBMS_OUTPUT.PUT_LINE('}');
            DBMS_OUTPUT.PUT_LINE('pedido = {nrotarj:string, nomtarj:string, fecha:string, codmenu:number, montoped:number, tipo:string, tipodoc:string, nroemp:number');
        END IF;
        FOR fila IN (SELECT * FROM PEDIDO) LOOP
            IF rowType THEN
                DBMS_OUTPUT.PUT_LINE('INSERT INTO PEDIDO (NROTARJ, NOMTARJ, FECHA, CODMENU, MONTOPED, TIPO, TIPODOC, NROEMP) VALUES ('||fila.nrotarj||', '''||fila.nomtarj||''', '''||fila.fecha||''', '||fila.codmenu||', '||fila.montoped||', '''||fila.tipo||''', '''||fila.tipodoc||''', '||fila.nroemp||');');
            ELSE
                DBMS_OUTPUT.PUT_LINE(fila.nrotarj||', '''||fila.nomtarj||''', '''||fila.fecha||''', '||fila.codmenu||', '||fila.montoped||', '''||fila.tipo||''', '''||fila.tipodoc||''', '||fila.nroemp);
            END IF;
        END LOOP;
        IF NOT rowType THEN
            DBMS_OUTPUT.PUT_LINE('}');
            DBMS_OUTPUT.PUT_LINE('partede = {codmenu:number, codmenucomponente:number');
        END IF;
        FOR fila IN (SELECT * FROM PARTEDE) LOOP
            IF rowType THEN
                DBMS_OUTPUT.PUT_LINE('INSERT INTO PARTEDE (CODMENU, CODMENUCOMPONENTE) VALUES ('||fila.codmenu||', '||fila.codmenucomponente||');');
            ELSE
                DBMS_OUTPUT.PUT_LINE(fila.codmenu||', '||fila.codmenucomponente);
            END IF;
        END LOOP;
        IF NOT rowType THEN
            DBMS_OUTPUT.PUT_LINE('}');
        END IF;
        rowType := FALSE;
    END LOOP;
END;
/